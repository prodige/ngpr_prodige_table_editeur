import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableViewComponent } from './components/table-view/table-view.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import {NgbModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { DynamicTemplateComponent } from './components/dynamic-template/dynamic-template.component';

import { EnvServiceProvider } from './services/env.service.provider';
import { MainComponent } from './components/main/main.component';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { EditDataComponent } from './components/edit-data/edit-data.component';
import { SortableHeaderDirective } from './directives/sortable-header.directive';
import { SaveInfoModalComponent, ModalConfirmComponent } from './modals/save-info-modal/save-info-modal.component';


@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    TableViewComponent,
    DynamicTemplateComponent,
    MainComponent,
    EditDataComponent,
    SaveInfoModalComponent,
    ModalConfirmComponent,
    SortableHeaderDirective
  ],

  entryComponents: [
    ModalConfirmComponent
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ModalConfirmComponent,
    SortableHeaderDirective
  ],
  providers: [EnvServiceProvider, NgbActiveModal],
  bootstrap: [AppComponent]
})
export class AppModule { }
