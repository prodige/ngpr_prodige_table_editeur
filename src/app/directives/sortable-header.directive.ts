import {Directive, EventEmitter, Input, Output, HostBinding, HostListener} from '@angular/core';

export type SortDirection = 'asc' | 'desc' | '';
const rotate: {[key: string]: SortDirection} = { asc: 'desc', desc: '', '': 'asc' };

export interface SortEvent {
  column: string;
  direction: SortDirection;
}

@Directive({
  selector: 'th[appSortable]',
})
export class SortableHeaderDirective {
  @HostBinding('class.asc') 'direction === "asc"';
  @HostBinding('class.desc') 'direction === "desc"';


  @Input() appSortable: string;
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  @HostListener('click')
  rotate(): void {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.appSortable, direction: this.direction});
  }
}
