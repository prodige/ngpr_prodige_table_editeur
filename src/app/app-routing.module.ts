import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { TableViewComponent } from './components/table-view/table-view.component';
import { EditDataComponent } from './components/edit-data/edit-data.component';

const routes: Routes = [
  {
    path: 'tableView/:uuid',
    component: MainComponent,

    children: [
      {
        path: '',
        component: TableViewComponent
      }, {
        path: 'edit',
        component: EditDataComponent
      }, {
        path: 'edit/:gid',
        component: EditDataComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tableView',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
