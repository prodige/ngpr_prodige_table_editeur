import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { DynamicTemplateComponent } from '../dynamic-template/dynamic-template.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends DynamicTemplateComponent  implements OnInit {
  constructor(protected dataService: DataService) {
    super(dataService);
    this.type = 'footer';
  }
}
