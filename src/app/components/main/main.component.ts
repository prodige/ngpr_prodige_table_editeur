import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { FeatureCollection } from 'src/app/models/layer-type';
import { TableViewDataService } from 'src/app/services/table-view-data.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [TableViewDataService, DecimalPipe, DataService]
})
export class MainComponent implements OnInit {
  /** */
  public title = '';

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService, private tableViewData: TableViewDataService, ) { }

  ngOnInit(): void {
    /** uuid */
    if (this.activatedRoute.snapshot.paramMap.get('uuid')) {
      this.dataService.uuid =  this.activatedRoute.snapshot.paramMap.get('uuid');
    }

    /** Right */
    this.dataService.getDataRight().subscribe( userRight => {
      this.tableViewData.setUserRight(userRight);
    });

    /** config */
    this.dataService.getContifData().subscribe( response => {
      if (response && response.metadata && response.metadata.title) {
        this.title = response.metadata.title;
      }
    });

    /** Enumeration */
    this.dataService.getEnum().subscribe(enumerations => {
      if (enumerations) {
        this.tableViewData.setEnumerations(enumerations);
      }
    });
  }

}
