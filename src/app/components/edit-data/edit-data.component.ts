import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Observable, forkJoin } from 'rxjs';
import { Feature } from 'src/app/models/layer-type';
import { Structure } from 'src/app/models/structure';
import { DataService } from 'src/app/services/data.service';
import { TableViewDataService } from 'src/app/services/table-view-data.service';

import { UserRight } from 'src/app/models/user-right';

import { ProdigeReponse } from 'src/app/models/prodige-reponse';
import { SaveInfoModalComponent } from 'src/app/modals/save-info-modal/save-info-modal.component';

@Component({
  selector: 'app-modal-edit',
  templateUrl: './edit-data.component.html',
  styleUrls: ['./edit-data.component.css'],
  // add NgbModalConfig and NgbModal to the component providers
  providers: [NgbModalConfig, NgbModal]
})
export class EditDataComponent implements OnInit {
  /** variable template */
  /** */
  public feature: Feature ;

  /** */
  public uuid: string;

  /** */
  public domainNameFile = {file: true, image: true };

  /** */
  public structures$: Observable<Structure[]>;

  /** */
  public enumerations: {};

  /** */
  public prodigeReponse: ProdigeReponse;

  /** */
  public regex = {
    email: '^[a-zA-Z]+[^@]*@[a-zA-Z]+[^@]*\\.[a-zA-Z]{2,}[^@]*$',
    phone_number: '^(\\+33|0)[1-9](\\d\\d){4}$'
  };

  /** */
  public userRight: UserRight;

  /** */
  @ViewChild(SaveInfoModalComponent) confirmModal: SaveInfoModalComponent;

  constructor(
    private tableViewData: TableViewDataService,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal
  ) {
    // customize default values of modals used by this component tree
    this.structures$ = this.tableViewData.getStructures$();

    this.tableViewData.getEnumerations$().subscribe( enumerations => {
      this.enumerations = enumerations;
    });

    this.tableViewData.getUserRight$().subscribe(userRight => {
      this.userRight = userRight;

      // si l'utilisateur n'a pas les droits d'édtion on renvoie sur la vue tableView
      if (userRight && !this.userRight.EDITION && !this.userRight.EDITION_MODIFICATION) {
        this.router.navigate(['/tableView/' + this.uuid, {}]);
      }
    });
  }

  /** */
  ngOnInit(): void {
    const gid =   this.activatedRoute.snapshot.paramMap.get('gid');
    this.uuid = this.dataService.uuid;

    if ( gid ) {
      forkJoin(
        this.dataService.getDataEditable(gid),
        this.dataService.getStructure()
      )
      .subscribe(val => {
        const result = this.dataService.postProcess(val[0], val[1]);
        this.tableViewData.setStructures( result.structures );

        if (result.features.length > 0) {
          this.feature = result.features[0];
        }
      });
    } else {
      this.dataService.getStructure().subscribe(structures => {
        const properties =  {};
        structures = this.dataService.apllyStructureFilter(structures);
        this.tableViewData.setStructures( structures );
        structures.forEach(structure => {
          properties[structure.column_name] = null;
        });

        this.feature =  {properties} as Feature;
        this.feature.properties.gid =  null;
      });

    }
  }


  /** */
  saveData(): void {
    if (this.feature.properties.gid || this.feature.properties.gid === '0') {
      this.dataService.updateData([this.feature]).subscribe(
        reponses => {
          const prodigeReponse = reponses && reponses.length > 0 ? reponses[0] : null;

          if (prodigeReponse.check) {
            this.router.navigate(['/tableView/' + this.uuid, {  }]);
          } else {
            this.confirmModal.openModal(reponses);
          }
        },
        reponseError => {
          this.confirmModal.openModal(null);
          console.error(reponseError);
        }
      );
    } else {
      this.dataService.postData([this.feature]).subscribe(
        reponses => {
          const prodigeReponse = reponses && reponses.length > 0 ? reponses[0] : null;

          if (prodigeReponse.check) {
            this.router.navigate(['/tableView/' + this.uuid, {  }]);
          } else {
            this.confirmModal.openModal(reponses);
          }
        },
        reponseError => {
          this.confirmModal.openModal(null);
          console.error(reponseError);
        }
      );
    }
  }
}

