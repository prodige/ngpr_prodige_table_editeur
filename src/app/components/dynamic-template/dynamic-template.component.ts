import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-dynamic-template',
  templateUrl: './dynamic-template.component.html',
  styleUrls: ['./dynamic-template.component.css']
})
export class DynamicTemplateComponent implements OnInit {
  /** Type: header / footer */
  protected type: string;

  /** */
  @Input() title: string;

  /** */
  public htmlContent: string;

  constructor(protected dataService: DataService) { }

  ngOnInit(): void {
    if (this.type) {
      this.htmlContent = '';
      this.dataService.loadHtmlContent(this.type)
      .then(
        htmlContent => {
          this.htmlContent = htmlContent.replace('{title}', this.title);
        },
        error => {
          console.error(error);
        }
      );
    }
  }
}
