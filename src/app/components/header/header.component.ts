import { Component, OnInit, Input } from '@angular/core';
import { DynamicTemplateComponent } from '../dynamic-template/dynamic-template.component';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends DynamicTemplateComponent  {
  constructor(protected dataService: DataService) {
    super(dataService);
    this.type = 'header';
  }
}
