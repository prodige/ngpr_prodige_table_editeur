import { Component, OnInit, ViewChildren, QueryList, Input } from '@angular/core';
import { Feature, FeatureCollection } from '../../models/layer-type';
import { SortableHeaderDirective, SortEvent } from 'src/app/directives/sortable-header.directive';
import { Observable, forkJoin, of } from 'rxjs';
import { TableViewDataService } from 'src/app/services/table-view-data.service';
import { Structure } from 'src/app/models/structure';
import { DataService } from 'src/app/services/data.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { UserRight } from 'src/app/models/user-right';
import { error } from 'protractor';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css'],
})
export class TableViewComponent implements OnInit {
  /** */
  public structures$: Observable<Structure[]>;

  /** Pour la pagination */
  public features$: Observable<Feature[]>;
  public total$: Observable<number>;
  public pageSizeChoice: Array<number>;

  /** droit utilisateur */
  public userRight$: Observable<UserRight>;
  public userRight: UserRight;

  /** */
  public isLoading = false;

  /** */
  public loadError = false;

  /** Pour le tri */
  @ViewChildren(SortableHeaderDirective) headers: QueryList<SortableHeaderDirective>;

  /** */
  constructor(
    public tableViewData: TableViewDataService,
    private  dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {
    this.features$ = this.tableViewData.getFeatures$();
    this.structures$ = this.tableViewData.getStructures$();
    this.total$ = this.tableViewData.getTotal$();
    this.userRight$ = this.tableViewData.getUserRight$();
  }

  /** */
  ngOnInit(): void {
    this.isLoading = true;
    this.userRight$.subscribe(userRight => {
      this.userRight =  userRight;
    });

    forkJoin(
      this.dataService.getData(),
      this.dataService.getStructure()
    )
    .subscribe(
      val => {
        const result = this.dataService.postProcess(val[0] as FeatureCollection, val[1] as Structure[]);
        this.tableViewData.initFeatures( result.features );
        this.tableViewData.setStructures( result.structures );
        this.isLoading = false;
        this.loadError = false;
      },
      errorMsg => {
        this.isLoading = false;
        this.loadError = true;
        console.error(errorMsg);
      }
    );

    this.tableViewData.searchFromUrlParam();

    this.pageSizeChoice = this.tableViewData.pageSizeChoice;

    const searchTerm: string = this.activatedRoute.snapshot.paramMap.get('q');
    const valueTerm: string = this.activatedRoute.snapshot.paramMap.get('value');
    const fieldFilter: string = this.activatedRoute.snapshot.paramMap.get('field');

    if (searchTerm) {
      this.tableViewData.searchTerm = searchTerm;
    } else if (valueTerm && fieldFilter) {
      this.tableViewData.searchTerm = searchTerm;
      this.tableViewData.sortColumn = fieldFilter;
    }

  }

  onSort({column, direction}: SortEvent): void {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.appSortable !== column) {
        header.direction = '';
      }
    });

    this.tableViewData.sortColumn = column;
    this.tableViewData.sortDirection = direction;
  }

  deleteData(feature: Feature): void {
    this.dataService.deleteData(feature).pipe(
      switchMap( response => this.dataService.getData() )
    )
    .subscribe(features => {
      const result = this.dataService.postProcess(features, this.tableViewData.getStructures());
      this.tableViewData.initFeatures( result.features );
    });
  }
}
