import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FeatureCollection, Feature } from '../models/layer-type';
import { EnvService } from './env.service';

import { Observable, Observer } from 'rxjs';
import { ConfigData } from '../models/config-data';
import { Structure } from '../models/structure';
import { ProdigeReponse } from '../models/prodige-reponse';
import { UserRight } from '../models/user-right';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private httpOption = {withCredentials: true, params: null};
  public uuid: string;

  constructor(
    private http: HttpClient,
    private envService: EnvService,
  ) {}

  /** */
  private columnNameHide = {_userid_creation: true , _userid_modification: true , _edit_datemaj: true,  _edit_datecrea: true};

  /** */
  getDataRight(): Observable<UserRight> {
    return this.http.get<UserRight>(
      this.envService.catalogueUrl +
      '/prodige/verify_rights?OBJET_TYPE=dataset&OBJET_STYPE&TRAITEMENTS=EDITION%7CNAVIGATION&uuid=' + this.uuid,
      this.httpOption
    );
  }

  /** Donnée tabulaire */
  getData(): Observable<FeatureCollection> {
    return this.http.get<FeatureCollection>( this.envService.catalogueUrl + '/api/data/' + this.uuid , this.httpOption);
  }

  /** Donnée tabulaire; en Edition */
  getDataEditable(gid: string): Observable<FeatureCollection> {
    const params = {nature: 'edit', gid};
    return this.http.get<FeatureCollection>( this.envService.catalogueUrl + '/api/data/' + this.uuid , {withCredentials: true, params});
  }

  /** Ajout de données tabulaire */
  postData(features: Array<Feature>): Observable<ProdigeReponse[]> {
    const data =  {type: 'FeatureCollection', features } as FeatureCollection;
    return this.http.post<ProdigeReponse[]>(this.envService.catalogueUrl + '/api/data_tabulaire/' + this.uuid, data, this.httpOption);
  }

  /** Mise à jour de données tabulaire */
  updateData(features: Array<Feature>): Observable<ProdigeReponse[]> {
    const data =  {type: 'FeatureCollection', features } as FeatureCollection;
    return this.http.patch<ProdigeReponse[]>(this.envService.catalogueUrl + '/api/data_tabulaire/' + this.uuid, data, this.httpOption);
  }

  /** Supression de données tabulaire */
  deleteData(feature: Feature): Observable<ProdigeReponse> {
    return this.http.delete<ProdigeReponse>(
      this.envService.catalogueUrl + '/api/data_tabulaire/' + this.uuid + '/' + feature.properties.gid,
      this.httpOption
    );
  }

  /** Config (pour le titre) */
  getContifData(): Observable<ConfigData> {
    const request = this.envService.geonetworkUrl + '/srv/fre/q?_content_type=json&fast=index&uuid=' + this.uuid;
    return this.http.get<ConfigData>(request);
  }

  /** Pour récuperrer les fichier html (header/footer) */
  loadHtmlContent(type: string): Promise<string> {
    const retour = '';

    return new Promise((resolve, reject) => {
      const url =  this.envService.frontCartoUrl +  this.envService.templatesTableUrl;
      this.http.get( url + this.uuid + '/' + type + '.html', {responseType: 'text'}).subscribe(data => {
        resolve(data);
      }, error => {
        console.error(error);
        this.http.get( url  + '/' + type + '.html', {responseType: 'text'}).subscribe(data => {
          resolve(data);
        });
      });
    });
  }

  /** Structure */
  getStructure(): Observable< Array<Structure> > {
    return this.http.get<Array<Structure>>( this.envService.catalogueUrl + '/api/structure/' + this.uuid, this.httpOption);
  }

  /** Enumeration */
  getEnum(): Observable<{}> {
    return this.http.get<{}>(this.envService.catalogueUrl + '/api/enum/' + this.uuid, this.httpOption);
  }

  /** Post process des données et structures */
  postProcess(data: FeatureCollection, structures: Structure[]): {features: Feature[], structures: Structure[]} {
    if (data.features && structures) {
      // traitement des structures
      structures = this.apllyStructureFilter(structures);

      // recherche des structures pour traiter les données
      const structuresFilter = structures.filter(structure => {
        return (structure.data_type === 'ARRAY' || structure.data_type === 'date') ;
      });

      // traitement des données
      const regex = new RegExp('[{}]', 'g');
      data.features.forEach((feature, index) => {
        structuresFilter.forEach(filter => {
          switch (filter.data_type) {
            case 'ARRAY':
              if (feature.properties[filter.column_name] && typeof feature.properties[filter.column_name] === 'string') {
                // conversion des tableaux sql '{}' en tableaux javascript []
                data.features[index].properties[filter.column_name] = feature.properties[filter.column_name].replace(regex, '').split(',');
              }
              break;
            case 'date':
              const date = moment(new Date(data.features[index].properties[filter.column_name]));
              data.features[index].properties[filter.column_name] = date.format('YYYY-MM-DD');
              break;
          }

        });
      });

      return {features: data.features, structures};
    }

    return {features: [], structures} ;
  }

  /** */
  apllyStructureFilter(structures: Structure[]): Structure[] {
    structures = structures.filter(structure => {
      return !this.columnNameHide[structure.column_name];
    });

    return structures;
  }
}
