import { TestBed } from '@angular/core/testing';

import { TableViewDataService } from './table-view-data.service';

describe('TableViewDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableViewDataService = TestBed.get(TableViewDataService);
    expect(service).toBeTruthy();
  });
});
