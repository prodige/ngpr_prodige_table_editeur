// tslint:disable: max-line-length
export class EnvService {
  // configuration appli
  public production = false;

  // url prodige
  public frontCartoUrl = 'https://frontcarto-prodige41.alkante.al:19021/';
  public catalogueUrl = 'https://catalogue-prodige41.alkante.al:34020/app_dev.php';
  public geonetworkUrl = 'https://prodige41.alkante.al:8443/geonetwork';

  // template
  public templatesTableUrl = 'IHM/cartes/table_templates/';

  // pour filtre les valeurs du tableau depuis l'url
  public fitlerOnAllColumn = 'q';
  public filterColumnField = 'field';
  public filterColumnValue = 'value';

  constructor() {}
}
