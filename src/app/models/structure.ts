export interface Structure {
  column_name: string;
  data_type: string;
  column_default: string;
  udt_name: string;
  character_maximum_length: string;
  is_nullable: boolean;
  domain_name: string;
  object_type: string;
  description: string;
}
