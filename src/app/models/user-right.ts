export interface UserRight {
  NAVIGATION: boolean;
  SYNCHRONIZE: boolean;
  EDITION_ATTRIBUTE_FILTER: boolean;
  EDITION: boolean;
  EDITION_AJOUT: boolean;
  EDITION_MODIFICATION: boolean;
  isAdmProdige: boolean;
  isConnected: boolean;
  userNom: string;
  userPrenom: string;
  userLogin: string;
  userId: number;
  userSignature: number;
  userEmail: string;
  success: boolean;
}
