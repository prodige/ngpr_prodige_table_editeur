import { FeatureCollection } from './layer-type';
export declare type Color = string | [number, number, number, number] | CanvasPattern | CanvasGradient;
export declare type GeoJsonGeometryTypes = Geometry['type'];
export declare type GeoJsonTypes = GeoJSON['type'];
export declare type BBox = [number, number, number, number] | [number, number, number, number, number, number];
export declare type Position = Array<number>;
export declare type GeoJSON = Geometry | Feature | FeatureCollection;
export declare type Geometry = Point | MultiPoint | LineString | MultiLineString | Polygon | MultiPolygon | GeometryCollection;
export declare type GeometryObject = Geometry;
export interface NotExclusive {
    [x: string]: any;
}
export interface GeoJsonObject extends NotExclusive {
    type: GeoJsonTypes;
    bbox?: BBox;
}
export interface Point extends GeoJsonObject {
    type: 'Point';
    coordinates: Position;
}
export interface MultiPoint extends GeoJsonObject {
    type: 'MultiPoint';
    coordinates: Position[];
}
export interface LineString extends GeoJsonObject {
    type: 'LineString';
    coordinates: Position[];
}
export interface MultiLineString extends GeoJsonObject {
    type: 'MultiLineString';
    coordinates: Position[][];
}
export interface Polygon extends GeoJsonObject {
    type: 'Polygon';
    coordinates: Position[][];
}
export interface MultiPolygon extends GeoJsonObject {
    type: 'MultiPolygon';
    coordinates: Position[][][];
}
export interface GeometryCollection extends GeoJsonObject {
    type: 'GeometryCollection';
    geometries: Geometry[];
}
export declare type GeoJsonProperties = {
    [name: string]: any;
} | null;
export interface Feature<G extends Geometry | null = Geometry, P = GeoJsonProperties> extends GeoJsonObject {
    type: 'Feature';
    geometry?: G;
    id?: string | number;
    properties: featureCollection.feature.Properties;
}
export interface FeatureCollection<G extends Geometry | null = Geometry, P = GeoJsonProperties> extends GeoJsonObject {
    type: 'FeatureCollection';
    id?: string | number;
    properties?: featureCollection.Properties;
    features: Array<Feature<G, P>>;
}
export declare namespace featureCollection {
    interface Properties extends NotExclusive {
        lang?: string;
        title?: string;
        subtitle?: string;
        date?: string;
        updated: Date;
        authors: Array<properties.Author>;
        links: properties.Links;
        extension?: properties.Extension;
        publisher?: string;
        generator?: properties.CreatorApplication;
        display?: properties.CreatorDisplay;
        rights?: string;
        categories?: Array<properties.Category>;
        gid?: string;
        tep: number;
    }
    namespace properties {
        interface Author extends NotExclusive {
            name: string;
            email?: string;
            uri?: string;
        }
        interface Links extends NotExclusive {
            profiles: Array<links.Via>;
            via?: Array<links.Via>;
        }
        namespace links {
            interface Via extends NotExclusive {
                href: string;
                type?: string;
                lang?: string;
                title?: string;
                length?: number;
            }
        }
        interface CreatorApplication extends NotExclusive {
            title?: string;
            uri?: string;
            version?: string;
        }
        interface CreatorDisplay extends NotExclusive {
            pixelWidth?: number;
            pixelHeight?: number;
            mmPerPixel?: number;
        }
        interface Category extends NotExclusive {
            term: string;
            scheme?: string;
            label?: string;
        }
        interface Extension extends NotExclusive {
            MapMinscaleDenominator: number;
            MapMaxscaleDenominator: number;
            MapDescription: string;
            MapPassword: string;
            MapBackgroundColor?: Color;
            Layout: extension.Layout;
            Tools: extension.Tools;
            LayerTree: extension.LayerTree;
        }
        namespace extension {
            interface Layout extends NotExclusive {
                Theme: string;
                PrintModels: Array<layout.PrintModel>;
                Copyright: layout.Copyright;
                ScaleImg: boolean;
                LogoURL: layout.LogoURL;
                ReferenceMap: boolean;
                ReferenceMapConfig: layout.ReferenceMapConfig;
                Legend: boolean;
            }
            namespace layout {
                interface PrintModel extends NotExclusive {
                    file: string;
                    title: string;
                }
                interface Copyright extends NotExclusive {
                    Text: string;
                    Size: number;
                    Font: string;
                    FontColor: Color;
                    BackgroundColor: Color;
                }
                interface LogoURL extends NotExclusive {
                    properties: Properties;
                }
                interface Properties extends NotExclusive {
                    links: Links;
                    bbox?: BBox;
                }
                interface Links extends NotExclusive {
                    previews: Array<Preview>;
                }
                interface Preview extends NotExclusive {
                    href: string;
                    type: string;
                    height: string;
                    width: string;
                }
                interface ReferenceMapConfig extends NotExclusive {
                    properties: Properties;
                }
            }
            interface Tools extends NotExclusive {
                AddLayer: boolean;
                AddLayerWMS: boolean;
                AddLayerWFS: boolean;
                Buffer: boolean;
                Context: boolean;
                Download: boolean;
                PrintImg: boolean;
                PrintBrowser: boolean;
                PrintPdf: boolean;
                FitAll: boolean;
                Info: boolean;
                InfoAttributesQuery: boolean;
                Measure: boolean;
                ScaleSelector: boolean;
                InfoTooltip: boolean;
                Zoom: boolean;
                Localisation: boolean;
                LocalisationConfig: Array<tools.LocalisationConfig>;
                Search: boolean;
                SearchAdressOpenLS: string;
                Geobookmark: boolean;
                GeobookmarkConfig: Array<tools.GeobookmarkConfig>;
                Annotation: tools.Annotation;
            }
            namespace tools {
                interface LocalisationConfig extends NotExclusive {
                    title: string;
                    layerId: string;
                    layerCodeField: string;
                    layerWFSUrl: string;
                    layerTextField: string;
                    criteriaRelated: number;
                    criteriaFieldRelated: string;
                    criteriaInSearch: boolean;
                }
                interface GeobookmarkConfig extends NotExclusive {
                    id?: string;
                    title: string;
                    bbox?: BBox;
                }
                interface Annotation extends NotExclusive {
                    Attribut: boolean;
                    AttributName: string;
                    Buffer: boolean;
                    BufferGeneral: boolean;
                    BufferGeneralDistance: number;
                    Export: boolean;
                    Point: boolean;
                    Line: boolean;
                    Polygon: boolean;
                    Circle: boolean;
                    Modify: boolean;
                    Move: boolean;
                    Snapping: boolean;
                    Styles: boolean;
                    Text: boolean;
                }
            }
            interface LayerTree extends NotExclusive {
                id: number;
                type: string;
                name: string;
                open: string;
                depth: string;
                opacity: string;
                children: Array<layerTree.LayerTreeGroup>;
            }
            namespace layerTree {
                interface LayerTreeGroup extends NotExclusive {
                    depth: number;
                    id: number | string;
                    name: string;
                    opacity: number;
                    open: boolean;
                    type: string | 'group';
                    children: Array<layerTreeGroup.Child>;
                }
                namespace layerTreeGroup {
                    interface Child extends NotExclusive {
                        id: number;
                        layerIdx: number;
                        name: string;
                        mapName: string;
                        layerName: string;
                        displayClass: boolean;
                        LayerDownload: boolean;
                        LayerLegendScaleDisplay: boolean;
                        LayerQuery: featureCollection.feature.properties.extension.LayerQuery;
                        baseLayer: boolean;
                        defaultbaseLayer: boolean;
                        depth: number;
                        forceOpacityControl: boolean;
                        imagebaseLayer: string;
                        opacity: number;
                        type: string | 'layer';
                        visible: boolean;
                    }
                }
            }
        }
    }
    namespace feature {
        interface Properties extends NotExclusive {
            title?: string;
            abstract?: string;
            updated: string | Date;
            author?: string;
            publisher?: string;
            rights?: string;
            date?: string | Date;
            active: boolean;
            offerings: Array<properties.Offering>;
            minscaledenominator?: number;
            maxscaledenominator?: number;
            links: properties.Links;
            categories: Array<featureCollection.properties.Category>;
            folder?: string;
            extension: properties.Extension;
            gid?: string;
            tep: number;
        }
        namespace properties {
            interface Offering extends NotExclusive {
                code: string;
                operations?: Array<offering.Operation>;
                contents?: Array<offering.Content>;
                styles?: Array<offering.Style>;
            }
            namespace offering {
                interface Operation extends NotExclusive {
                    code: string;
                    method: string;
                    type?: string;
                    href: string;
                    request?: Content;
                    result?: Content;
                }
                interface Content extends NotExclusive {
                    type: string;
                    content?: string;
                    href?: string;
                    title?: string;
                }
                interface Style extends NotExclusive {
                    name: string;
                    title: string;
                    abstract?: string;
                    default?: boolean;
                    legendURL?: string;
                    content?: Content;
                }
            }
            interface Links extends NotExclusive {
                alternates?: Array<links.Via>;
                previews?: Array<links.Via>;
                data?: Array<links.Via>;
                via?: links.Via;
            }
            namespace links {
                interface Via extends NotExclusive {
                    href: string;
                    type?: string;
                    lang?: string;
                    title?: string;
                    length?: number;
                }
            }
            interface Extension extends NotExclusive {
                LayerGroup: number;
                LayerOrder: number;
                LayerDownload: boolean;
                LayerLegendScaleDisplay: boolean;
                LayerQuery: extension.LayerQuery;
            }
            namespace extension {
                interface LayerQuery extends NotExclusive {
                    URLConfig: string;
                    Fields: Array<layerQuery.Field>;
                }
                namespace layerQuery {
                    interface Field extends NotExclusive {
                        FieldName: string;
                        FieldAlias: string;
                        FieldType: string;
                        FieldFormat: string;
                        FieldDateTooltip: boolean;
                        FieldURLConfig?: any;
                    }
                }
            }
        }
    }
}
