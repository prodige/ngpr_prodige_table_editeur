export interface ProdigeReponse {
  check: boolean;
  msg: string;
}
