import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ProdigeReponse } from 'src/app/models/prodige-reponse';


@Component({
  templateUrl: './save-info-modal.component.html'
})
export class ModalConfirmComponent {
  /** */
  modalReady = false;

  /** */
  prodigeReponse: ProdigeReponse;

  constructor(
    private config: NgbModalConfig,
    public modalRef: NgbActiveModal
  ) {
    this.config.backdrop = 'static';
    this.config.keyboard = false;
  }
}
@Component({
  selector: 'app-save-info-modal',
  template: `<!-- modal à ouvrir -->`,
  styleUrls: ['./save-info-modal.component.css'],
  entryComponents: [ModalConfirmComponent]
})
export class SaveInfoModalComponent implements OnInit {

  /** */
  modalRef: NgbModalRef = null;

  constructor(private modalService: NgbModal, ) { }

  /** */
  ngOnInit(): void {

  }

  /** */
  openModal(prodigeReponses: ProdigeReponse[]): void {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static'
    });

    const modalInstance: ModalConfirmComponent = this.modalRef.componentInstance;
    modalInstance.modalReady = true;
    modalInstance.prodigeReponse = prodigeReponses && prodigeReponses.length > 0 ? prodigeReponses[0] : null;
  }

}
