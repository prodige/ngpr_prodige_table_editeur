import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveInfoModalComponent } from './save-info-modal.component';

describe('SaveInfoModalComponent', () => {
  let component: SaveInfoModalComponent;
  let fixture: ComponentFixture<SaveInfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
